package com.digicoreltd;
import com.healthmarketscience.jackcess.*;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class ConvertMDBTOCSV {

    public void getTable() {
        try {

            //New file is the mdb file you're reading from from, getTable is the the table name
            Table table = DatabaseBuilder.open(new File("EJournal_181123121055.mdb")).getTable("Events");
            //new file writer is the name of the file you want to create
            CSVWriter csvWriter = new CSVWriter(new FileWriter("example1.csv"));

            ArrayList<String> header = new ArrayList<String>();

            if (table != null) {
                System.out.println("Table " + table.getName());
                System.out.println("Table " + table.getColumnCount());
                System.out.println("Table " + table.getRowCount());
                int count = 1;
                for (Row row : table) {
//                    System.out.println("Column 'Module' has value: " + row.get("Module"));
                    System.out.println();
                    if (count == 1 ) {
                        header.addAll(row.keySet());
                        String[] header1 = header.toArray(new String[0]);
                        System.out.println(Arrays.toString(header1));
                        csvWriter.writeNext(header1);
                    }
                    count++;
//                    String[] data = {Arrays.toString(row.values().toArray()).replace('[', ' ').replace(']', ' ').trim()};

                    String[] data = new String[] {Arrays.toString(row.values().toArray())};

                    System.out.println(Arrays.toString(data));

                    csvWriter.writeNext(data);
                }
                csvWriter.close();
            } else {
                System.out.println("No content in file");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
