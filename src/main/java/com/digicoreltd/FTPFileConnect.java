package com.digicoreltd;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FTPFileConnect  {
    public void ftpConnector() {
        ServerReply serverReply = new ServerReply();
        FTPClient ftpClient = new FTPClient();
        FileOutputStream fileOutputStream = null;

        try {
            System.out.println("-------Connecting To FTP Server-------");

            //replace with ftp servewr url
            ftpClient.connect("host name here");
//            ftpClient.connect("ftp.gnu.org");

            System.out.println("-------Checking server reply-------");

            serverReply.showServerReply(ftpClient);

            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                System.out.println("Operation failed. Server reply code: " + replyCode);
                return;
            } else {
                System.out.println("Operation successful. Server reply code: " + replyCode);
            }

            System.out.println("-------Attempting FTP Server Login-------");
            //provide username and password for ftp server
            boolean isSuccess = ftpClient.login("username here", "password here");
            serverReply.showServerReply(ftpClient);

            if (isSuccess) {
                System.out.println("-------Successfully Logged In-------");
                // Create an OutputStream for the file to download
                String filename = ".cphorde";
                String[] files = ftpClient.listNames();
                //check for file in this directory
                FTPFile[] directories = ftpClient.listDirectories("/");
                fileOutputStream = new FileOutputStream(filename);
                // Fetch file from server
                System.out.println("-------Downloading File From Server-------");
                ftpClient.retrieveFile("/public_html" + filename, fileOutputStream);
                System.out.println("-------File Downloaded-------");
                System.out.println("-------Creating Directory On FTP-------");
                //create directory in ftp server
                ftpClient.makeDirectory("/testingFTP");
                System.out.println("-------Directory Created-------");
                int count = 1;

                if (directories != null && directories.length > 0) {
                    for (FTPFile aFile : directories) {
                        System.out.println("File " + count + " :" + aFile);
                        count++;
                    }

                } else {

                    throw new FileNotFoundException("No file /  directory here");

                }
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (fileOutputStream != null) {

                    fileOutputStream.close();

                }

                System.out.println("Logging Out");
                ftpClient.disconnect();
                System.out.println("Successfully Logged Out");

            } catch (IOException e) {

                e.printStackTrace();

            }
        }
    }

}
